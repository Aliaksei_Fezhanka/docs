# Autocode Course Development

Find an actual version of the guide here:\
[https://git.epam.com/epm-rdpw/doc/-/blob/master/management/course-development.md](https://git.epam.com/epm-rdpw/doc/-/blob/master/management/course-development.md)

Find an init version of the guide made by Evgenii Efimchik:\
[https://git.epam.com/Evgenii_Efimchik/docs/-/blob/master/autocode-instructions/autocode-development.md](https://git.epam.com/Evgenii_Efimchik/docs/-/blob/master/autocode-instructions/autocode-development.md)

Autocode is a tool to manage and deliver programming exercises.

Autocode checks student solutions automatically using unit tests and static analysis.

## Autocode Objects
Autocode allows managing **Courses** and **Groups**.

## Courses
Autocode Course represents a structured set of exercises for some educational program.

A Course has following attributes: 
- Name
- Description
- Skill - Options: Java, JS, .Net, Golang, Python, Kotlin
- Build Tool - Defined by Skill, e.g. Java may use Gradle or Maven, JS - npm.
- Location - EPAM Office location or Online option
- Quality profile - Profile of static analysis
- Default build limit - how many times a student may apply with his solution for a task by default

A Course may be in DRAFT, PUBLISHED and CLOSED states. Here are brief details:

Courses statuses and actions:

|  | DRAFT | PUBLISHED | CLOSED |
| ------ | ------ | ------ | ------ | 
| Editing | + | + | - |
| Delivering | + | + | - |
| Publishing | + | - | - |
| Cloning | + | + | + |
| Cloning with Git | + | + | + |

Courses contain **Topics** - groups of tasks. Topic has its own name and description.

**Task** represent a single exercise. A Task has following attributes: 
- Name
- Description
- Minimal score - Minimal score value that would be considered a success.
- Available from/till - Dates of task availability. Optional.
- Task visibility - Flag to hide/show task in the course
- Skeleton link - Link to a GitLab repository containing skeleton project of the exercise
- Internal tests link - Link to a GitLab repository containing project tests

## Creating a Course
Consider a Course to be a structured set of exercises. A Course contains no specification of how to apply it.
So, the only considerations while creating a course is how to structure tasks.

Creating a course you must also specify a **Skill** (refers to program language) and a **Build Tool** (e.g., Maven or Gradle for Java Skill).
You have to use the chosen environment for all course tasks.

As **Location** values set office where the course takes place or choose **Online** if applicable.

**Quality Profile** is a set of instructions to SonarQube for static analysis.
It is OK to use standard *Autocode* option.

**Default build limit** is a default amount fo tries student has to complete a task of the course.
Task attributes may override this value.

Trainer can add to course/topic/task description tables, pictures, links, format text, etc. using the Rich text editor.

## Creating Tasks
A Course contains topics and topics contain tasks.
Topics are there just to structure a course. Tasks are more complex and described below. 
Provide each element a description to let students and other trainers better understand course structure and details.

A task refer to a project managed by chosen build tool.
You should provide unit tests with it.
Some secret tests may be hidden from students in separate special project.

To perform an exercise defined in the task student has to press the [Start] button, skeleton repository is forked automatically, made as private, and made it available to Autocode.
After that, he can commit solution changes in his fork repository and ask Autocode to check it.

There is an option to reuse already existing tasks instead of recreating them.
To do that you may hit [Add task] button instead of [Create task]. 
Then you need to choose a task to import in your course.
You may reuse whole topics not just tasks.

### Skeleton/Internal tests link
Creating a task you have to specify at least **Skeleton link** - URL of repository containing skeleton project.

Also, you may specify **Internal tests link**. It contains URL of repository containing unit tests for the project.
If you do not specify it, tests of skeleton project would be used for unit testing.

Do not worry student may pass unit testing step by simply deleting all the tests.
Autocode cares about it and takes all test-related code from the initial skeleton repository or hidden tests repository if it is specified.

**Note:** URL should link the repository itself and not its web page. Here is an example:
    
    https://gitlab.com/a-course-group/a-task-repository.git

### Minimal score      
Creating a task you have to provide a **Minimal score**.
It is important since every score equal or above this value would be considered by Autocode as a success. To choose a proper value you should consider the standard score formula of Autocode by default or change it using the widget for configurable build scoring for tasks.

| Evaluation step | Max score |
| --- | --- |
| INIT | 0 |
| CHECKOUT | 10 |
| COMPILE | 10 |
| UNIT_TESTS | 50 |
| SONAR | 30 |
| SONAR ISSUE | -1 |
| **Total** | **100** |

- CHECKOUT step is successful if Autocode has access to student exercise repository.
- COMPILE step is successful if the project compile command performs OK.
- UNIT_TESTS is for passing all the unit tests provided for the exercise. Note, that score may be partial here: student will get __50 * (green tests count / all tests count)__.
- SONAR refers to a SonarQube static analysis. This score may be partial as well: the more code smells a solution has, the less score the student get.  
- SONAR ISSUE value is a Penalty for every code issue. So, by the default SONAR ISSUE = -1, but is’s also configurable.

So, you may set **100** as a minimal score to ensure students not just make their code work but also it is clean enough to raise no SonarQube alarm.

If you are not planning to use static analysis to check solutions, you may set minimal score value to **70**.

You may use a **10** or **20** value to mark successful solutions that just compiles.  

If you want use both tests and analysis and would like your students to have some room you may set some other value, like **90**.
Be aware, though, that you cannot specify threshold per check step, only the total one.  

### Build limit
Set value of maximum attempts for the task.

In general case, when student has enough tests to check his solution locally, 
Autocode team recommends to use *5* attempts limit,
so a student would be mindful interpreting static analysis results
and considering cases when submitted solution is too expensive and does not comply with check time limit.

Choose value considering task complexity and what tests students may run locally before submitting a solution.
Other factors could be whether you encourage them to use GitLab Web IDE or run tests locally before the submission.

Setting number of attempts to *1* may be too harsh even if students have all the tests in skeleton repository.
There are situations when tests pass locally but fail at Autocode due to environment differences.
On the other hand, setting it to very high value like hundreds and thousands
could make a task effectively have no attempt limit
and that might encourage students to make too many submissions that could result into high load on Autocode
and therefore increase time of an attempt being in a queue.

### Visibility and Availability
You may hide a task by setting *Task Visibility* flag.
May help if the task is under construction or review.

*Availability* dates define start and end of a period when a task is available to students.
These settings are optional. Group settings may override specified values. 

## Mastering a task repository
Let us dive into how to develop good repositories for your tasks, so it would work with Autocode smoothly.
First, usually it takes more than one repository to supply an Autocode task.

### Skeleton repository
Skeleton repository is one that forked when a student performs an exercise. It should contain skeleton code introducing an API for a student to implement. Also, it should contain unit tests, so a student would be able to check his solution locally.

### Internal tests repository
- You may put tests in separate repository, so it is hidden from students.In this case, such a repository must be private.
- Another approach is to use a skeleton repository as internal tests repository. It is suitable in case all tests may be provisioned to students.
Anyway, Autocode gets test code for automated checks from internal tests repository, so student cannot pass a task by editing provisioned tests.

### Evaluating a task
Once task is created, you may consider checking your own reference solution to ensure it behaves as expected.

There is such special operation **[Check task]** available to lab managers.
To check a task you provide following: 
- *Skill* (e.g. Java)
- *Build* tool (e.g. maven)
- *Task repository* (put here the reference implementation repo URL)
- *Task branch* (branch containing solution)
- *Tests repository* (put here the internal tests repo URL)
- *Tests branch* (branch containing tests)

You have to specify *Task repository* - URL to the project containing reference solution.
You may also specify different *Tests repository*, and you need to specify branches for both repositories.

Skeleton repository usually should be private so all its forks would be private as well.
In order to let Autocode access a private reference implementation repo, please, add *@autocodebot* as a *Reporter* to project repository.

Having configured the environment you may submit a check attempt, and it will pass all the checking steps so at least you can ensure that a successful solution exists.

**Note:**
- Add @autocodebot as a "Reporter" to the project to let Autocode access to a private skeleton repository in case you prefer using a private repository and do not already create a group,
- Add @autocodebot as a Reporter to the project to let Autocode access to a private skeleton repository if you have group with "None" provider,
- @autocodebot will be added automatically to all task repositories (skeleton and test) of your course if you have group with "Gitlab" provider,
- You do not need to add @autocodebot in case you prefer using the public task repository

## Advices
- **Make skeleton code be as minimal as possible.**
    That will encourage students to be more creative. 
    Also, that may greatly result if you are going to use some plagiarism detection tools.

- **Provide detailed task description in README.md in the repository.**
    Do not rely on Autocode description only.
    That would make your exercises more independent and exportable.
    Remember, in case your task places in several courses,
    it will be way simpler to change description in a single repository
    than in each Autocode task.  

- **Provide unit tests covering most cases in skeleton repo.**
    Even if you use separate repository for tests
    you should also provide some tests covering main cases in skeleton project as well.
    Otherwise, they would not have a clue if their code meets requirements until submitting it to Autocode.
    Autocode check is long-running job, so students would have to wait on each attempt and such experience is frustrating.
    
- **Remember, you may introduce integration tests.**
    Even if an exercise is quite sophisticated and employs databases, web services or IO routines, there is usually a solution to help you test it:
    - database-related code - DBUnit,
    - web-service code - Spring MockMVC, RestClient, RestUnit,
    - external service dependencies - testcontainers.

    Also, remember that a lot of tests may employ mocks (Mockito, PowerMock...) instead of real dependencies.
    One more reason to care about it is Autocode check time limits.

- **Be aware of time limits**
    Autocode use time limits for its check routines in order to handle endless execution problem 
    and remain responsive even under significant load.
    Currently, it is capped to 30 minutes, but this 30 minutes limit a whole routine including initialization, repo checkout, compiling, static analysis and running tests. 
    So, if your exercise includes long-running initialization or execution or extensive test usage,
    it is strongly recommended to check does it work well with reference implementation
    (see description of "Check task" feature above in Evaluating a task section).
    You may also set your own, more granular tim limits within a global one with help of Timeout attributes, 
    which are usually provided by test frameworks. E.g. JUnit 5 allows constructions like `@Test(timeout = 500)`.
    
- **If you use group with NONE git-provider use GitLab Groups to grant access to skeleton repositories**
    Imagine you have a course of 10 tasks assigned to a group of 100 students.
    If you grant access personally (adding students to each project as Reporters)
    that will result in 1,000 operations.
    Otherwise, you can form a GitLab group, add students there just once
    and then assign Reporter role in a skeleton project to the whole group.

## Groups
Autocode Group refers to a set of students passing specified course. In order to assign a group to a specified course, course status should be "Published". Moreover, the students will be allowed to see the course on the course list. 

Before creation group with Giltab provider make sure that:
- Your account is integrated with Gitlab (Click View profile in user menu, Press button INTEGRATE GITLAB);
- The course owner and group owner is the same user (you);
- The course owner is the owner of all task repositories;
- Course status is “Published”.

A Group has following attributes: 
- Name
- Description
- Course - A course that is linked to the group
- Start/End date - dates of course
- Pipeline type - Options: *Full*/*Sonar*/*Unit*. Choose a pipeline type to check solutions: Unit testing, SonarQube static analysis or both.
- Education type
    - Choose *Virtual class* option for the group in its classical conception: group may start only after all its participants have joined and no more students allowed after the group start.
    - Choose *Self-paced, eLearning* to allow launching the group with no students but with possibility for their following subscription after group start.
- Subscription type - Available of *Self-paced, eLearning* education type. 
    - Choose *Auto Assign* to let student proceed to the group materials immediately after registration.
    - Choose *Manual Assign* to let the trainer confirm (or reject, accordingly) student assignment to the group.
- Git provider
    - None
    - Gitlab

**Note:** choosing Gitlab provider is recommended.

Group with Gitlab provider:  
- After creating a group @autocodebot will be added automatically to all task repositories (skeleton and test) of course tasks.  

Group with None provider:  
- After creating a group @autocodebot will NOT be added automatically to all task repositories (skeleton and test) of course tasks. 
- Use public project or add student as a member with a "Reporter" role to the private project. 
- To let ‘Autocode’ access a private skeleton repo add @autocodebot as a Reporter to project members. 

### Group states
A Group may be in states DRAFT, NOT STARTED, IN PROGRESS, FINISHED or CANCELLED.
Description of a Group lifecycle goes below.

|  | DRAFT | NOT STARTED | IN PROGRESS | FINISHED | CANCELLED |
| ------ | ------ | ------ | ------ |  ------ |  ------ | 
| Open registration | + | - | - | - | - |
| Cancel | + | - | - | - | - |
| Start the group | - | + | - | - | - |
| Finish | - | - | + | - | - |

- New groups are in DRAFT state. Lab manager may perform following operations:
    - open registration, then it becomes NOT STARTED,
    - cancel, then it becomes CANCELLED.
- NOT STARTED groups are open for student registration. Lab manager may perform following operation:
    - start the group, then it becomes IN PROGRESS.
- A group IN PROGRESS serves tasks of the attached course to students. Lab manager may perform following operation:
    - finish the group, then it becomes FINISHED
- FINISHED and CANCELLED do not serve but store student results.

### Permissions
Course owner (or Admin) is able to **add another trainer to the group to manage group/course.** Course owner (or Admin) can set/change permissions to the trainer assigned to the group.

There are **2 type of permission** – Group_management and Course_management.
- **0 permission:** 
    - Trainer is able to read course
    - Trainer is able to read group 
    - Trainer is able to get access to the  student repository
- **Group_management permission:**
    - Trainer is able to read course 
    - Trainer is able to manage group
    - Trainer is able to get access to the  student repository
    - Trainer can restore student with “Left”/”Excluded” status
- **Course_management permission:**
    - Trainer is able to manage course 
    - Trainer is able to read group 
    - Trainer is able to get access to the  student repository
- **Group_management + Course_management:**
    - Trainer is able to manage course 
    - Trainer is able to manage group
    - Trainer is able to get access to the student repository
    - Trainer can restore student with “Left”/”Excluded” status

## Student repository 
Once a student clicks on the [Start] button, skeleton repository is forked, this fork becomes a student repository and Autocode considers it contains a student solution.

To perform an exercise defined in task student must click on the [Start] button, after that:   
- Skeleton task repository is forked automatically
- @autocodebot is added automatically to student repository
- The repository is made private  

**Note:** (None git provider group) 
- Check a task repository is not private, or the student is added as a member with a "Reporter" role to the private project. After that student can commit solution changes in his fork repository and ask ‘Autocode’ to check it.  

‘Autocode’ insists that all student repositories should be private to prevent plagiarism. On the other hand, ‘Autocode’ should have access to these repositories as well. That is why @autocodebot is added automatically to student repository. 

### Getting access to student repository
There is an option to get access to student's repository for Admin or Trainer. Click [Get access] button on student task details page when he has active repository and you will be added as Reporter to his repository.

### Resetting student results
There are 2 ways to clear student results for Admins and Trainers – **Reset group or student results**.

- It is possible to clear student result with **Reset student progress** button on student task details page. 
- It is possible to clear group result with **Reset group progress** button on task details page.

This option is available in case:
1. Group status is In progress
2. Task is started by the student
3. Course management permission is added to Trainer.

After resetting the results course owner receive an email with details and data about the Trainer who reset the progress. 

## Course Cloning with Git
To see and clone the course you **should have Course_management permission** on that course.
 - **To clone course with git make sure that:**
    - Your account is integrated with Gitlab (Click View profile in user menu, press button [INTEGRATE GITLAB]);
     - Course owner account is integrated with Gitlab;
     - !**The visibility of task repositories** (skeleton and test repositories) is **public** or **you are added as a member** with Reporter role to course owner's repositories when visibility of repositories is **private**.
- **Steps** - Course details -> Operations -> Clone with Git
- **Result**:
    - Course is cloned.
    - All task repositories are forked.
    - All task links of New Cloned course are updated to forked repositories.
    - You are the new owner of forked repositories (skeleton and tests). 
    - Visibility of all forked repositories is the same as in owner course.
- **Warning!** When cloning own course, the repositories are not forked. 

## Reporting
Reports are available in the system by Reports tab.
You can view: Overview Statistics, How Students take the Courses, Students Statistics, Top Student Score.